var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'bootstrap.min.css',
        'css3_reset.css',
        'custom.css',
    ]);
    mix.scripts([
        'jquery.min.js',
        'bootstrap.min.js',
    ]);
    mix.copy('resources/assets/images', 'public/img');
    mix.copy('resources/assets/fonts', 'public/fonts');
});
