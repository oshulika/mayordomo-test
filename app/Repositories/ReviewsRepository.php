<?php
namespace App\Repositories;

use \App\Abstracts\Repository as AbstractRepository;

class ReviewsRepository extends AbstractRepository implements ReviewsRepositoryInterface
{
    // This is where the "magic" comes from:
    protected $modelClassName = 'App\Reviews';
    
    // This class only implements methods specific to the UserRepository
    public function getByUsername($username)
    {
        $where = call_user_func_array("{$this->modelClassName}::findOrFail", array($id));

        return $where->get();
    }

    // This class only implements methods specific to the UserRepository
    public function getReviewsForProduct($product_id)
    {
        $where = call_user_func_array("{$this->modelClassName}::where", array($product_id));
        return $where->get();
    }
}