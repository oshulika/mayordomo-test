<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{

    protected $table = 'reviews';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description','username','product_id'
    ];
}
