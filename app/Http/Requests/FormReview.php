<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FormReview extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        \Validator::extend('unique_multiple', function ($attribute, $value, $parameters, $validator)
        {
            //if this is for an update then don't validate
            //todo: this might be an issue if we allow people to "update" one of the columns..but currently these are getting set on create only
            if (isset($validator->getData()['id'])) return true;

            // Get table name from first parameter
            $table = array_shift($parameters);

            // Build the query
            $query = \DB::table($table);

            // Add the field conditions
            foreach ($parameters as $i => $field){
                $query->where($field, $validator->getData()[$field]);
            }

            // Validation result will be false if any rows match the combination
            return ($query->count() == 0);

        });
        return [
            'title'=>'required|unique_multiple:reviews,title,username|max:200',
            'description'=>'required|max:1000',
            'username'=>'required|unique_multiple:reviews,title,username|max:200',
        ];
    }
}
