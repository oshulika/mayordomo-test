<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReviewsTest extends TestCase
{
    /**
     * A homepage must return status 200.
     *
     * @return void
     */
    public function testHomepage()
    {
        $response = $this->call('GET', '/');
        $this->assertEquals(200, $response->status());
    }

    /**
     * A homepage must return status 200.
     *
     * @return void
     */
    public function testCreateReview()
    {
           $response = $this->call('POST', '/reviews', [
                'username' => 'testAbigail',
                'title' => 'testAbigail',
                'description' => 'testAbigail',
           ]);
           $this->seeInDatabase('reviews', ['username' => 'testAbigail']);
    }
}
