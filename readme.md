# Mayordomo test

This is a test repo for Mayordomo. It's just a small task to check out overall HTML/CSS/JS/PHP/Laravel skills. There's no problem if you don't know these things well; learning what we don't know is just as important as knowing what we do. So, relax and enjoy.

The goal of this test is to build a system that lets us create and view reviews for a product or website. Using Laravel, you'll need to create a table, add a form, save the reviews and display them on a page. We have a design that we'll need to build in HTML using Bootstrap.

1. Fork this repo, and clone to your local machine
2. Run `composer install` and get everything installed

## Tasks

1. Create a migration using artisan. Using the image in this repo (if there is no image then ask Dan for it), decide which fields would be useful and add them to the migration. Run the migration to create the table
2. Create a model for the review table
3. Create a route to be able to let us add/edit reviews, and a page to list all reviews
4. Create a form to be used for both the add and edit pages, including errors for invalid fields. Create those pages and get them working
5. When editing and adding reviews, create a "form request" so we can define which fields are required and how we validate them
    1. In the form request, make sure that the same review cannot be posted twice (that is to say that the same text cannot be submitted for the same user; this will get tricky when editing a review)
6. Build the publicly visible page based off the design in the repo. You don't need to build the header and footer, but the reviews should be responsive in two columns and then narrow down to one on mobile. Set up and use Bootstrap for this. As the McLaren F1 team say, "Good enough is not good enough". We want the design to look exactly like the image, so spend time making sure it looks and works correctly
7. For CSS, use the Gulp file that comes with Laravel so the CSS is optimised and minimised automatically. Don't include the CSS files separately, use Laravel's built in system
8. Write some tests to:
    1. Make sure the page of reviews are available and returning a code `200`
    2. That when editing and adding a review, the changes are visible in the database