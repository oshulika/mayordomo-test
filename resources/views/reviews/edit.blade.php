@extends('layouts.app')

@section('content')
      <div class="row " style="padding:100px">
        <div class="col-lg-6 col-lg-offset-3 review-form">
        {!! Form::model($review, array('route' => array('reviews.update', $review->id),'method'=>'put')) !!}
                @include('reviews._form')
            {!! Form::close()!!}
        </div>
      </div>
@endsection




