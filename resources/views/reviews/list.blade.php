@extends('layouts.app')

@section('content')

      <div class="row review_row text-center">
        <a href="{{URL::to('/reviews/create')}}"><img src="img/review.png" alt="review"></a>
        <h3><span>Reviews</span></h3>
      </div>

      <ul class="items_list">
      @foreach($reviews as $review)
        <li class="text-center">
          <div class="item text-left">
            <div class="circle-wrap">
              <div class="circle">
                <a href="{{URL::to('reviews/'.$review->id.'/edit')}}"><i class='glyphicon glyphicon-option-horizontal'></i></a>
              </div>
            </div>
            <p>
              {{$review->description}}
            </p>
            <span class="item_label">{{$review->username}}</span>
          </div>
        </li>
        @endforeach
        </ul>

@endsection