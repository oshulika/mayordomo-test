@extends('layouts.app')

@section('content')
      <div class="row " style="padding:100px">
        <div class="col-lg-6 col-lg-offset-3 review-form">
            {!! Form::open(array('route' => array('reviews.store'),'method'=>'post')) !!}
                @include('reviews._form')
            {!! Form::close()!!}
        </div>
      </div>
@endsection

