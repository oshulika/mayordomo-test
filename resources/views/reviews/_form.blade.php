<div class="box-body form-horizontal">
    <div class="form-group">
    <label for="title" class="col-sm-3 control-label">Title</label>
    <div class="col-sm-9">
    {!! Form::text('title', null, ['id' => 'title','class'=>'form-control']) !!}
    </div>
    </div>
    <div class="form-group">
        <label for="username" class="col-sm-3 control-label">Username</label>
        <div class="col-sm-9">
        {!! Form::text('username', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Description</label>
        <div class="col-sm-9">
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="box-footer">
    <a href="{{ url('/') }}" class="btn btn-default">Back</a>
    {!! Form::submit('Save',['class'=>'btn btn-info pull-right'])!!}
</div>